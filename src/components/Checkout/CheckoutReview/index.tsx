import Button from '@/components/Button';
import Headline from '@/components/Headline';
import StripePayment from '@/components/StripePayment';
import { useCartContext } from '@/context/CartContext';
import { useCheckoutContext } from '@/context/CheckoutContext';
import { useState } from 'react';
import { FaApplePay } from 'react-icons/fa';
import { FiEdit2 } from 'react-icons/fi';
import { CheckoutSummary } from '../CheckoutSummary';

export const CheckoutReview = () => {
  const {
    setActiveStep,
    setIsEdit,
    formData,
    selectedPayment,
    setStartCheckout,
    setCheckoutComplete,
  } = useCheckoutContext();

  const { checkout, paymentSuccessFull } = useCartContext();

  return (
    <div className="pt-10 space-y-10">
      <Headline centered>Review Order</Headline>

      <div className="space-y-6">
        <div className="flex justify-between">
          <h4 className="text-xl font-semibold">Shipping address</h4>
          <FiEdit2
            className="w-5 h-5 cursor-pointer text-bluey"
            onClick={() => {
              setActiveStep('Shipping');
              setIsEdit(true);
            }}
          />
        </div>
        <div className="py-4 pl-4 border pr-36 border-light-gray">
          <p className="font-bold">
            {formData?.firstName} {formData?.lastName}
          </p>
          <p>
            {formData?.address} ({formData?.aptFloorSuite}), {formData?.zipCode} {formData?.city}{' '}
            United Kingdom, {formData?.phone}
          </p>
        </div>
      </div>

      <div className="w-full space-y-6">
        <div className="flex justify-between">
          <h4 className="text-xl font-semibold">Payment</h4>
          <FiEdit2
            className="w-5 h-5 cursor-pointer text-bluey"
            onClick={() => {
              setActiveStep('Payment');
              setIsEdit(true);
            }}
          />
        </div>
        <div className="p-4 border border-light-gray">
          <div className="flex items-center justify-between">
            <label
              className="inline-flex items-center text-sm font-medium text-left"
              htmlFor="paypal"
            >
              <input
                type="checkbox"
                className="rounded-full focus:ringfocus:ring-offset-0 focus:ring-bluey focus:ring-opacity-0"
                value={selectedPayment?.name}
                checked
                readOnly
              />
              <span className="ml-2">{selectedPayment?.name}</span>
            </label>
            {selectedPayment?.icon}
          </div>

          <StripePayment />
        </div>
      </div>

      <CheckoutSummary />

      <div className="flex flex-col items-center justify-center w-full pt-6 space-y-6">
        <Button
          id="place-order"
          type="submit"
          secondary
          onClick={() => {
            checkout();

            setTimeout(() => {
              setCheckoutComplete(true);
              setStartCheckout(false);
            }, 2000);
          }}
          disabled={!paymentSuccessFull}
        >
         Place order
        </Button>
        <a
          className="block pb-20 underline cursor-pointer"
          onClick={() => setActiveStep('Payment')}
        >
          &#60; Back
        </a>
      </div>
    </div>
  );
};
