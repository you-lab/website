import Breadcrumb from '@/components/Breadcrumb';
import Headline from '@/components/Headline';
import { useCheckoutContext } from '@/context/CheckoutContext';
import { useModal } from '@/hooks/useModal';
import { useEffect } from 'react';
import Modal from '../Modal';
import { CheckoutForm } from './CheckoutForm';
import { CheckoutPayment } from './CheckoutPayment';
import { CheckoutReview } from './CheckoutReview';

const Checkout = () => {
  const { steps, activeStep, startCheckout, setStartCheckout, setActiveStep } =
    useCheckoutContext();

  if (!startCheckout) {
    return null;
  }

  return (
    <Modal
      open={startCheckout}
      close={() => {
        setStartCheckout(false);
        setActiveStep('Shipping');
      }}
      fullWidth
    >
      <div className="max-w-md mx-auto ">
        <div className="space-y-6">
          <Headline>Checkout</Headline>
          <Breadcrumb breadCrumbSteps={steps} />
        </div>

        {activeStep === 'Shipping' && (
          <div className="w-full">
            <div className="w-full mx-auto">
              <CheckoutForm />
            </div>
          </div>
        )}

        {activeStep === 'Payment' && (
          <div className="w-full">
            <div className="w-full mx-auto">
              <CheckoutPayment />
            </div>
          </div>
        )}

        {activeStep === 'Review' && (
          <div className="w-full">
            <div className="w-full mx-auto">
              <CheckoutReview />
            </div>
          </div>
        )}
      </div>
    </Modal>
  );
};

export default Checkout;
