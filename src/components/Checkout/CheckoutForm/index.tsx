import Button from '@/components/Button';
import Headline from '@/components/Headline';
import { CheckoutFormData, useCheckoutContext } from '@/context/CheckoutContext';
import { useForm } from 'react-hook-form';

export const CheckoutForm = () => {
  const { setActiveStep, formData, saveFormData, isEdit } = useCheckoutContext();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ defaultValues: formData });

  const onSubmit = (data: CheckoutFormData) => {
    saveFormData(data);

    if (!isEdit) {
      setActiveStep('Payment');
    } else {
      setActiveStep('Payment');
      setActiveStep('Review');
    }
  };

  return (
    <div className="flex flex-col pt-10 space-y-10">
      <Headline centered>Where’s this order going?</Headline>
      <form className="space-y-4" onSubmit={handleSubmit(onSubmit)}>
        <div className="relative grid floating-input">
          <input
            defaultValue="England"
            className="w-full h-12 p-3 focus:outline-none focus:border-bluey focus:shadow-sm border-light-gray disabled:text-mid-gray"
            type="text"
            placeholder="Shipping to:"
            name="country"
            disabled
          />
          <label
            htmlFor="shipping_to:"
            className="absolute top-0 left-0 h-full px-3 py-3 transition-all duration-100 ease-in-out origin-left transform pointer-events-none text-mid-gray"
          >
            Shipping to:
          </label>
        </div>
        <div className="grid grid-cols-2 gap-3 ">
          <div className="relative grid floating-input">
            <input
              className={`w-full h-12 p-3 focus:outline-none focus:border-bluey focus:shadow-sm border-light-gray  ${
                errors.firstName ? 'border-red-500' : ''
              }`}
              type="text"
              placeholder="First name"
              {...register('firstName', { required: true, maxLength: 80 })}
            />
            <label
              htmlFor="first_name"
              className="absolute top-0 left-0 h-full px-3 py-3 transition-all duration-100 ease-in-out origin-left transform pointer-events-none"
            >
              First Name
            </label>
          </div>

          <div className="relative grid floating-input">
            <input
              className={`w-full h-12 p-3 focus:outline-none focus:border-bluey focus:shadow-sm border-light-gray  ${
                errors.lastName ? 'border-red-500' : ''
              }`}
              type="text"
              placeholder="Last name"
              {...register('lastName', { required: true, maxLength: 100 })}
            />
            <label
              htmlFor="last_name"
              className="absolute top-0 left-0 h-full px-3 py-3 transition-all duration-100 ease-in-out origin-left transform pointer-events-none"
            >
              Last Name
            </label>
          </div>
        </div>
        <div>
          <div className="relative grid floating-input">
            <input
              className={`w-full h-12 p-3 focus:outline-none focus:border-bluey focus:shadow-sm border-light-gray  ${
                errors.address ? 'border-red-500' : ''
              }`}
              type="text"
              placeholder="Address"
              {...register('address', { required: true })}
            />
            <label
              htmlFor="address"
              className="absolute top-0 left-0 h-full px-3 py-3 transition-all duration-100 ease-in-out origin-left transform pointer-events-none"
            >
              Address
            </label>
          </div>
        </div>
        <div>
          <div className="relative grid floating-input">
            <input
              className={`w-full h-12 p-3 focus:outline-none focus:border-bluey focus:shadow-sm border-light-gray  ${
                errors.aptFloorSuite ? 'border-red-500' : ''
              }`}
              type="text"
              placeholder="Apt / Floor / Suite"
              {...register('aptFloorSuite', { required: true })}
            />
            <label
              htmlFor="apt_floor_suite"
              className="absolute top-0 left-0 h-full px-3 py-3 transition-all duration-100 ease-in-out origin-left transform pointer-events-none"
            >
              Apt / Floor / Suite
            </label>
          </div>
        </div>

        <div className="grid grid-cols-2 gap-3">
          <div className="relative grid floating-input">
            <input
              className={`w-full h-12 p-3 focus:outline-none focus:border-bluey focus:shadow-sm border-light-gray  ${
                errors.city ? 'border-red-500' : ''
              }`}
              type="text"
              placeholder="City"
              {...register('city', { required: true })}
            />
            <label
              htmlFor="city"
              className="absolute top-0 left-0 h-full px-3 py-3 transition-all duration-100 ease-in-out origin-left transform pointer-events-none"
            >
              City
            </label>
          </div>
          <div className="relative grid floating-input">
            <input
              className={`w-full h-12 p-3 focus:outline-none focus:border-bluey focus:shadow-sm border-light-gray  ${
                errors.zipCode ? 'border-red-500' : ''
              }`}
              type="text"
              placeholder="ZIP Code"
              {...register('zipCode', { required: true })}
            />
            <label
              htmlFor="zip_code"
              className="absolute top-0 left-0 h-full px-3 py-3 transition-all duration-100 ease-in-out origin-left transform pointer-events-none"
            >
              ZIP Code
            </label>
          </div>
        </div>
        <div>
          <div className="relative grid floating-input">
            <input
              className={`w-full h-12 p-3 focus:outline-none focus:border-bluey focus:shadow-sm border-light-gray  ${
                errors.phone ? 'border-red-500' : ''
              }`}
              type="tel"
              placeholder="Phone"
              {...register('phone', {
                required: true,
                maxLength: 11,
                pattern: /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/,
              })}
            />
            <label
              htmlFor="phone"
              className="absolute top-0 left-0 h-full px-3 py-3 transition-all duration-100 ease-in-out origin-left transform pointer-events-none"
            >
              Phone
            </label>
          </div>
        </div>

        <div className="flex justify-center">
          <Button type="submit" secondary className="mt-6">
            {!isEdit ? 'Next' : 'Update'}
          </Button>
        </div>
      </form>
    </div>
  );
};
