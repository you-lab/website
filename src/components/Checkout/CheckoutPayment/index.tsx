import Button from '@/components/Button';
import Headline from '@/components/Headline';
import { useCartContext } from '@/context/CartContext';
import { useCheckoutContext } from '@/context/CheckoutContext';
import { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { FaCcPaypal } from 'react-icons/fa';
import { VscCreditCard } from 'react-icons/vsc';
/* 
import { usePaymentInputs, PaymentInputsWrapper } from 'react-payment-inputs';
//@ts-ignore
import images from 'react-payment-inputs/images'; */

export const CheckoutPayment = () => {
  const [payments, setPayments] = useState([
    {
      id: 'credit-card',
      name: 'credit-card',
      displayName: 'Credit/Debit Card',
      icon: <VscCreditCard className="w-10 h-10" />,
      checked: true,
      disabled: false,
    },
    {
      id: 'paypal',
      name: 'paypal',
      displayName: 'PayPal',
      icon: <FaCcPaypal className="w-10 h-10" />,
      checked: false,
      disabled: true,
    },
    /* {
      id: 'google-pay',
      name: 'googlePay',
      displayName: 'Google Pay',
      icon: <FaApplePay className="w-10 h-10" />,
      checked: false,
    },
    {
      id: 'apple-pay',
      name: 'applePay',
      displayName: 'Apple Pay',
      icon: <FaApplePay className="w-10 h-10" />,
      checked: false,
    }, */
  ]);

  const [paymentsRequired, setPaymentsRequired] = useState<boolean>(true);
  const [selectedPayment, setSelectedPayment] = useState<string>();

  /*  const { wrapperProps, getCardImageProps, getCardNumberProps, getExpiryDateProps, getCVCProps } =
    usePaymentInputs(); */

  const { setActiveStep, savePayment, formData } = useCheckoutContext();

  const { applyCustomerData, order, prepareStripePayment } = useCartContext();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = (data: any) => {
    applyCustomerData(formData!, data.emailAddress);

    setActiveStep('Review');
    prepareStripePayment();
  };
  const choosePayment = (payment: any) => {
    setSelectedPayment(payment); /* setPayments(

    /*     savePayment({ name: payment.dispayName, icon: payment.icon });
     */

    /*
      payments.map((item) => {
        if (item.id === id) {
          item.checked = true;
        } else {
          item.checked = false;
        }
        return item;
      })
    ); */
  };

  /*   const uncheckPayments = () => {
    setPaymentsRequired(false);
    return setPayments(
      payments.map((item) => {
        item.checked = false;
        return item;
      })
    );
  };

  const onCardNumberChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    console.log(e?.target?.value);
  };

  const onExpiryDateChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    console.log(e.target.value);
  };

  const onCVCChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    console.log(e.target.value);
  }; */

  const checkIfOneSelected = () => {
    const isBelowThreshold = (item: any) => item.checked === false;

    return payments.every(isBelowThreshold);
  };

  useEffect(() => {
    savePayment({ name: payments[0].displayName, icon: payments[0].icon });
  }, []);

  return (
    <div className="pt-10 space-y-10">
      <Headline centered>What’s your payment information?</Headline>
      {/*  <div className="w-full space-y-4">
        <label htmlFor="card_number">Enter your card information</label>
        <PaymentInputsWrapper {...wrapperProps}>
          <svg
            //@ts-ignore
            {...getCardImageProps({ images })}
          />
          <div className="flex justify-between" onClick={() => uncheckPayments()}>
            <input
              className="border-transparent focus:border-transparent focus:ring-0"
              {...getCardNumberProps({ onChange: onCardNumberChange })}
            />
            <input
              className="border-transparent focus:border-transparent focus:ring-0"
              {...getExpiryDateProps({ onChange: onExpiryDateChange })}
            />
            <input
              className="border-transparent focus:border-transparent focus:ring-0"
              {...getCVCProps({ onChange: onCVCChange })}
            />
          </div>
        </PaymentInputsWrapper>
      </div> */}

      {/*   <div className="relative w-full">
        <hr className="mt-[3px]" />
        <p className="absolute p-2 -translate-x-1/2 -translate-y-1/2 bg-white left-1/2 top-1/2">
          or
        </p>
      </div> */}
      <form className="space-y-4" onSubmit={handleSubmit(onSubmit)}>
        <div className="w-full">
          {payments.map((payment) => (
            <div
              className={`flex items-center justify-between ${
                payment.disabled
                  ? 'text-mid-gray text-opacity-50 cursor-not-allowed pointer-events-none'
                  : ''
              }`}
              key={payment.id}
            >
              <label
                className="inline-flex items-center text-sm font-medium text-left"
                htmlFor="paypal"
              >
                <input
                  type="checkbox"
                  className={`rounded-full focus:ringfocus:ring-offset-0 focus:ring-bluey focus:ring-opacity-0 ${
                    errors[payment.name] && checkIfOneSelected() === true ? 'border-red-500' : ''
                  }`}
                  defaultValue={payment.name}
                  checked={payment.checked}
                  onClick={() => choosePayment(payment)}
                  /* {...register(payment.name, { required: paymentsRequired })} */
                  readOnly
                />
                <span className="ml-2">{payment.displayName}</span>s
              </label>
              {payment.icon}
            </div>
          ))}

          <p className="text-xs text-mid-gray">PayPal is coming soon...</p>
        </div>
        <div className="w-full space-y-4">
          <p>Where should we send your receipt?</p>
          <div className="relative grid floating-input">
            <input
              className={`w-full h-12 p-3 focus:outline-none focus:border-bluey focus:shadow-sm border-light-gray  ${
                errors.emailAddress ? 'border-red-500' : ''
              }`}
              type="text"
              placeholder="Email Address"
              defaultValue={order?.customer_email}
              {...register('emailAddress', {
                required: true,
                pattern:
                  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
              })}
            />
            <label
              htmlFor="emailAddress"
              className="absolute top-0 left-0 h-full px-3 py-3 transition-all duration-100 ease-in-out origin-left transform pointer-events-none"
            >
              Email Address
            </label>
          </div>
        </div>
        <div className="flex flex-col items-center justify-center space-y-6">
          <Button type="submit" secondary>
            Next
          </Button>

          <a className="block underline cursor-pointer" onClick={() => setActiveStep('Shipping')}>
            &#60; Back
          </a>
        </div>
      </form>
    </div>
  );
};
