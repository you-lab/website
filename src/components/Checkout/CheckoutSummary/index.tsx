import { useCartContext } from '@/context/CartContext';

export const CheckoutSummary = () => {
  const { lineItems, order } = useCartContext();

  if (lineItems === undefined || order === undefined) {
    return null;
  }

  return (
    <div className="w-full space-y-4 text-lg">
      <h4 className="text-xl font-semibold">Order summary</h4>

      {lineItems.map((item) => (
        <div key={item.id}>
          {item.item_type === 'skus' && (
            <div className="flex items-center space-x-4" key={item.id}>
              <img className="w-16 h-16 aspect-square" src={item.image_url} alt={item.name} />
              <div className="flex justify-between w-full">
                <p className="text-raisin-black">
                  {item.quantity} x {item.name} <br />{' '}
                  <span className="text-sm">Laundry Detergent</span>
                </p>
                <p className="text-mid-gray">{item.formatted_total_amount}</p>
              </div>
            </div>
          )}
        </div>
      ))}
      <div className="flex justify-between">
        <p className="font-semibold text-raisin-black">Shipping costs</p>
        <p className="text-mid-gray">{order.formatted_shipping_amount}</p>
      </div>
      <div className="flex justify-between">
        <p className="font-semibold text-raisin-black">TAX </p>
        <p className="text-mid-gray">{order.formatted_total_tax_amount}</p>
      </div>
      <div className="flex justify-between">
        <p className="font-semibold text-raisin-black">Coupon </p>
        <p className="text-mid-gray">{order.formatted_discount_amount}</p>
      </div>
      <hr />
      <div className="flex justify-between">
        <p className="text-xl font-semibold text-raisin-black">Total </p>
        <p className="text-xl font-semibold text-raisin-black">{order.formatted_total_amount}</p>
      </div>
    </div>
  );
};
