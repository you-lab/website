import { useEffect, useState } from 'react';

type Counter = {
  initialQuantity: number;
  updateQuantity: (quantity: number) => void;
};

const Counter = ({ initialQuantity, updateQuantity }: Counter) => {
  const [quantity, setQuantity] = useState<number>(initialQuantity);

  useEffect(() => {
    updateQuantity(quantity);
  }, [quantity]);

  return (
    <div className="flex items-center px-4 text-lg border-2 border-black">
      <button onClick={() => (quantity > 1 ? setQuantity(quantity - 1) : null)} type="button">
        <svg
          className="w-4 h-4 text-black"
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          stroke="currentColor"
        >
          <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M18 12H6" />
        </svg>
      </button>
      <span className="w-[60px] text-center">{quantity}</span>
      <button onClick={() => (quantity < 10 ? setQuantity(quantity + 1) : null)} type="button">
        <svg
          className="w-4 h-4 text-black"
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          stroke="currentColor"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth={2}
            d="M12 6v6m0 0v6m0-6h6m-6 0H6"
          />
        </svg>
      </button>
    </div>
  );
};

export default Counter;
