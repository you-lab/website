const Image = ({ url }: { url: string }) => {
  return <img className="max-w-sm aspect-square" src={url} />;
};

export default Image;
