import { loadStripe } from '@stripe/stripe-js';
import { useStripe, useElements, CardElement, Elements } from '@stripe/react-stripe-js';
import Button from '../Button';
import { useCartContext } from '@/context/CartContext';

// TODO: Use Stripe Public Key via env variable here
const stripePromise = loadStripe(
  "process.env.STRIPE_PUBLIC_KEY"
);

const StripePayment = () => {
  return (
    <Elements stripe={stripePromise}>
      <CardForm />
    </Elements>
  );
};

const options = {
  style: {
    base: {
      fontSize: '16px',
      color: '#424770',
      letterSpacing: '0.025em',
      fontFamily: 'Source Code Pro, monospace',
      '::placeholder': {
        color: '#aab7c4',
      },
    },
    invalid: {
      color: '#9e2146',
    },
  },
};

const CardForm = () => {
  const stripe = useStripe();
  const elements = useElements();

  const { stripePaymentClientSecret, confirmPaymentIntent } = useCartContext();

  const handleSubmit = async (event: any) => {
    event.preventDefault();

    if (!stripe || !elements || !stripePaymentClientSecret) {
      // Stripe.js has not loaded yet. Make sure to disable
      // form submission until Stripe.js has loaded.
      return;
    }

    const { paymentIntent, error } = await stripe.confirmCardPayment(stripePaymentClientSecret, {
      payment_method: {
        type: 'card',
        //@ts-ignore
        card: elements.getElement(CardElement),
      },
    });

    console.log({paymentIntent});


    if (error) {
      // Inform the customer that there was an error.
      console.log(error.message);
    } else {
      // Handle next step based on PaymentIntent's status.
      //@ts-ignore
      console.log('PaymentIntent ID: ' + paymentIntent.id);
      //@ts-ignore
      console.log('PaymentIntent status: ' + paymentIntent.status);
      confirmPaymentIntent();
    }

  };

  return (
    <form className="py-6 space-y-6" onSubmit={handleSubmit}>
      <label>
        <CardElement
          options={options}
          /* onChange={(event) => {
           if(event.complete) {
             confirmPaymentIntent();
           }
          }} */
        />
      </label>
      <Button secondary fullWidth type="submit" disabled={!stripe}>
        Check Credit Card Details
      </Button>
    </form>
  );
};

export default StripePayment;
