const Steps = ({ step }: { step: number }) => {
  return (
    <div className="space-x-2">
      <span>Step</span>
      <span>{step}</span>
      <span>/</span>
      <span>2</span>
    </div>
  );
};

export default Steps;
