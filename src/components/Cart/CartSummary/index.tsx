import { useCartContext } from '@/context/CartContext';
import { CartCoupon } from '../CartCoupon';

export const CartSummary = ({
  subTotalAmount,
  shippingCosts,
  taxAmount,
  couponCode,
  couponAmount,
}: {
  subTotalAmount: string;
  shippingCosts: string;
  taxAmount: string;
  couponCode: string;
  couponAmount: string;
}) => {
  const { removeCoupon } = useCartContext();
  return (
    <div className="py-6 space-y-4">
      <div className="flex justify-between">
        <p>Sub Total</p>
        <p>{subTotalAmount}</p>
      </div>
      <div className="flex justify-between">
        <p>
          Shipping costs <sup>*</sup>
        </p>
        <p>--</p>
      </div>
      <div className="flex justify-between">
        <p>TAX</p>
        <p>{taxAmount}</p>
      </div>
      <p>
        <sup>*</sup>...to be calculated in checkout
      </p>
      <div className="space-y-4">
        {couponCode && (
          <>
            <div className="flex justify-between">
              <p className="text-bluey">{couponCode}</p>
              <div className="flex space-x-2">
                <p className="text-bluey">{couponAmount}</p>
                <button
                  onClick={() => {
                    removeCoupon();
                  }}
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="w-4 h-4 text-bluey"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth={1}
                      d="M6 18L18 6M6 6l12 12"
                    />
                  </svg>
                </button>
              </div>
            </div>
          </>
        )}
        <CartCoupon />
      </div>
    </div>
  );
};
