import Button from '@/components/Button';
import { useCartContext } from '@/context/CartContext';
import { Coupon } from '@commercelayer/sdk';
import { useState, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { FiChevronDown, FiChevronUp } from 'react-icons/fi';

export const CartCoupon = () => {
  const [select, setSelect] = useState(false);
  const [coupon, setCoupon] = useState('');

  const [availableCoupons, setAvailableCoupons] = useState<String[]>();
  const { applyCoupon, couponCodes } = useCartContext();

  const {
    register,
    handleSubmit,
    setError,
    formState: { errors },
  } = useForm();

  useEffect(() => {
    if (couponCodes) {
      setAvailableCoupons(couponCodes.map((coupon: Coupon) => (coupon.code ? coupon.code : '')));
    }
  }, [couponCodes]);

  const onSubmit = () => {
    if (availableCoupons?.includes(coupon)) {
      applyCoupon(coupon);
    } else {
      setError('coupon', { type: 'custom' });
    }
  };

  return (
    <div className="space-y-4">
      <div
        className="flex items-center justify-between cursor-pointer"
        onClick={() => setSelect(!select)}
      >
        <h5 className="font-semibold text-bluey">Apply Coupon</h5>

        {!select ? (
          <FiChevronDown className="w-6 h-6 text-bluey" />
        ) : (
          <FiChevronUp className="w-6 h-6 text-bluey" />
        )}
      </div>

      {select && (
        <form className="space-y-4" onSubmit={handleSubmit(onSubmit)}>
          <div className="relative grid floating-input">
            <input
              className={`w-full h-12 p-3 focus:outline-none focus:border-bluey focus:shadow-sm border-light-gray  ${
                errors.coupon ? 'border-red-500' : ''
              }`}
              type="text"
              placeholder="Coupon"
              {...register('coupon')}
              onChange={(e) => setCoupon(e.target.value)}
            />
            <label
              htmlFor="coupon"
              className="absolute top-0 left-0 h-full px-3 py-3 transition-all duration-100 ease-in-out origin-left transform pointer-events-none"
            >
              Coupon
            </label>
            {errors.coupon && (
              <p className="mt-2 text-sm text-red-500">Please enter a valid Coupon</p>
            )}
          </div>
          <Button secondary fullWidth type="submit">
            Apply
          </Button>
        </form>
      )}
    </div>
  );
};
