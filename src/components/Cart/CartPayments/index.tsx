import PaymentButton from '@/components/PaymentButton';

export const CartPayments = () => {
  return (
    <>
      <PaymentButton.PayPal />
      <PaymentButton.ApplePay />
      <PaymentButton.GooglePay />
    </>
  );
};
