import { useCartContext } from '@/context/CartContext';
import { useState, useEffect } from 'react';

export const CartInfo = () => {
  const { order } = useCartContext();

  return (
    <div className="pb-10 space-y-4">
      <p>
        You are £ {order && order.total_amount_float! < 20 ? 20 - order.total_amount_float! : '0'}
        {''} away from free standard shipping. Subscription current Orders ship free.
      </p>
      <CartSlider />
    </div>
  );
};

const CartSlider = () => {
  const [progress, setProgress] = useState(0);
  const { order, lineItems } = useCartContext();

  const caluclateProgress = () => {
    const total = order?.total_amount_float;

    //@ts-ignore
    const totalProgress = (parseInt(total) / 20) * 100;

    setProgress(totalProgress);
  };

  useEffect(() => {
    caluclateProgress();
  }, [lineItems, order]);

  return (
    <div className="w-full h-[10px] bcurrentOrder rounded-full bg-white border-black border">
      <div
        className={`${
          progress === 35 ? 'w-[35%]' : progress === 75 ? 'w-[75%]' : 'w-full'
        } h-[8px] bg-blue-600 rounded-full border-white border`}
      ></div>
    </div>
  );
};
