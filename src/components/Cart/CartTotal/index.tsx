export const CartTotal = ({ totalAmount }: { totalAmount: string }) => {
  return (
    <div className="space-y-8">
      <hr />
      <div className="flex justify-between">
        <p>Estimated Total</p>
        <p>{totalAmount}</p>
      </div>
    </div>
  );
};
