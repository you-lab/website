import Counter from '@/components/Counter';
import { CartItems, useCartContext } from '@/context/CartContext';

export const CartItem = ({
  id,
  name,
  description,
  image_url,
  quantity,
  formatted_total_amount,
}: CartItems) => {
  const { updateQuantity, removeLineItem } = useCartContext();

  return (
    <div>
      <div className="flex justify-end w-full">
        <button
          onClick={() => {
            removeLineItem(id);
          }}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="w-6 h-6 text-gray-500"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={1}
              d="M6 18L18 6M6 6l12 12"
            />
          </svg>
        </button>
      </div>
      <div className="flex items-center pr-10 space-x-10">
        <img className="w-28 h-28 aspect-square" src={image_url} alt="" />
        <div>
          <h4 className="text-lg font-semibold">{name}</h4>
          <p>Laundry Detergent</p>
        </div>
      </div>
      <div className="flex items-center justify-between mt-6">
        <Counter initialQuantity={quantity} updateQuantity={(e) => updateQuantity(id, e)} />
        <p>{formatted_total_amount}</p>
      </div>
      <hr className="my-10" />
    </div>
  );
};
