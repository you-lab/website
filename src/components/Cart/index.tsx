import { useEffect, useState } from 'react';
import { Dialog } from '@headlessui/react';
import Button from '../Button';
import { useCartContext } from '@/context/CartContext';

import { LineItem, Order } from '@commercelayer/sdk';
import { useCheckoutContext } from '@/context/CheckoutContext';
import { CartInfo } from './CartInfo';
import { CartItem } from './CartItem';
import { CartSummary } from './CartSummary';
import { CartTotal } from './CartTotal';

declare global {
  interface Window {
    braintree: any;
  }
}

const Cart = ({ close }: { close: () => void }) => {
  const [isOpen, setIsOpen] = useState(true);
  const [currentOrder, setCurrentOrder] = useState<Order>();
  const { lineItems, getOrder, getLineItems, order } = useCartContext();
  const { setStartCheckout } = useCheckoutContext();

  useEffect(() => {
    setCurrentOrder(order!);
  }, [order]);

  useEffect(() => {
    getLineItems();
  }, []);

  /*   useEffect(() => {
    const braintree = window.braintree || {};
    const button = document.querySelector('#submit-button') as HTMLElement;

    if (braintree) {
      braintree.dropin
        .create({
          authorization: 'sandbox_mfwjpmgk_dd85dmd5pmg3r84q',
          container: '#dropin-container',
        })
        .then((instance: any) => {
          button.addEventListener('click', () => {
            instance.requestPaymentMethod((requestPaymentMethodErr: any, payload: any) => {
              console.log({ requestPaymentMethodErr, payload });
              checkout(payload.nonce);
              setTimeout(() => {
                close();
              }, 1500);
            });
          });
        });
    }
  }, [window, lineItems]); */

  return (
    <Dialog
      open={isOpen}
      onClose={() => {
        setIsOpen(false);
        close();
      }}
      className="fixed inset-0 flex items-center justify-center z-[1000]"
    >
      <Dialog.Overlay className="fixed inset-0 bg-black bg-opacity-30" />

      <div className="fixed right-0 w-full h-full max-w-md p-8 mx-auto space-y-4 overflow-y-scroll bg-white shadow-lg lg:p-16 lg:max-w-md">
        <div className="flex justify-between">
          <Dialog.Title className="text-2xl">My Cart ({currentOrder?.skus_count})</Dialog.Title>
          <div className="">
            <button
              onClick={() => {
                close();
                setIsOpen(false);
              }}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="w-8 h-8 text-gray-500"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth={1}
                  d="M6 18L18 6M6 6l12 12"
                />
              </svg>
            </button>
          </div>
        </div>
        <CartInfo />

        {lineItems?.map((item: LineItem) => (
          <div key={item.sku_code}>
            {item.item_type === 'skus' && (
              <CartItem
                id={item.id}
                sku_code={item.sku_code!}
                name={item.name!}
                quantity={item.quantity!}
                description={''}
                image_url={item.image_url!}
                formatted_total_amount={item.formatted_total_amount!}
              />
            )}
          </div>
        ))}

        <CartSummary
          subTotalAmount={currentOrder?.formatted_subtotal_amount!}
          shippingCosts={currentOrder?.formatted_shipping_amount!}
          taxAmount={currentOrder?.formatted_total_tax_amount!}
          couponCode={currentOrder?.gift_card_or_coupon_code!}
          couponAmount={currentOrder?.formatted_discount_amount!}
        />
        <CartTotal totalAmount={currentOrder?.formatted_total_amount!} />
        <div className="flex items-center mb-4">
          <label className="inline-flex items-center text-sm text-left text-mid-gray">
            <input
              type="checkbox"
              className="rounded-full focus:ringfocus:ring-offset-0 focus:ring-bluey focus:ring-opacity-0"
              required
            />
            <span className="ml-2">Subscribe to newsletter & special offers</span>
          </label>
        </div>

        {/* <div id="dropin-container"></div> */}
        {/*  <CartPayments /> */}

        <div className="flex justify-center w-full pt-4">
          <Button
            /* id="submit-button" */ secondary
            id="checkout"
            className="w-full"
            onClick={() => {
              setStartCheckout(true);
              close();
            }}
          >
            Checkout
          </Button>
        </div>

        <p className="text-sm text-center text-mid-gray">
          By checking out, I accept the Terms & Conditions
        </p>
      </div>
    </Dialog>
  );
};

export default Cart;
