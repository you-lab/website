import { Dialog } from '@headlessui/react';

const Modal = ({
  open,
  close,
  fullWidth,
  title,
  description,
  large,
  children,
}: {
  open: boolean;
  close: () => void;
  title?: string;
  description?: string;
  fullWidth?: boolean;
  large?: boolean;
  children: React.ReactNode;
}) => {
  return (
    <>
      {fullWidth ? (
        <Dialog
          className="fixed inset-0 z-[9999] flex justify-center h-full py-24 overflow-y-scroll bg-white"
          open={open}
          onClose={() => {
            close();
          }}
        >
          <div className="mx-auto">
            <div className="flex justify-end w-full">
              <button
                onClick={() => {
                  close();
                }}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="w-8 h-8 text-gray-500"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={2}
                    d="M6 18L18 6M6 6l12 12"
                  />
                </svg>
              </button>
            </div>

            {children}
          </div>
        </Dialog>
      ) : (
        <Dialog
          className="fixed inset-0 flex items-center justify-center"
          open={open}
          onClose={() => {
            close();
          }}
        >
          <Dialog.Overlay className="fixed inset-0 bg-black bg-opacity-30" />

          <div
            className={`fixed ${
              large
                ? 'lg:max-w-[600px] space-y-10 p-8 lg:px-20'
                : 'lg:max-w-md space-y-4 p-8 lg:p-10'
            }  mx-auto text-center bg-white shadow-lg `}
          >
            <div className="flex justify-end w-full">
              <button
                onClick={() => {
                  close();
                }}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="w-8 h-8 text-gray-500"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={2}
                    d="M6 18L18 6M6 6l12 12"
                  />
                </svg>
              </button>
            </div>

            {title && <Dialog.Title className="text-2xl font-semibold">{title}</Dialog.Title>}
            {description && <Dialog.Description>{description}</Dialog.Description>}
            {children}
          </div>
        </Dialog>
      )}
    </>
  );
};

export default Modal;
