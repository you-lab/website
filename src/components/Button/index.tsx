import React from 'react';
import tw, { styled } from 'twin.macro';

type ButtonProps = {
  primary?: boolean;
  secondary?: boolean;
  subscribe?: boolean;
  active?: boolean;
  isOutOfStock?: boolean;
  isPayPal?: boolean;
  isApplePay?: boolean;
  isGooglePay?: boolean;
  unstyled?: boolean;
  fullWidth?: boolean;
} & React.ComponentPropsWithoutRef<'button'>;

const Button = ({
  primary,
  secondary,
  subscribe,
  isPayPal,
  isApplePay,
  active,
  isOutOfStock,
  unstyled,
  fullWidth,
  ...rest
}: ButtonProps) => {
  return (
    <StyledButton
      className={primary ? 'button' : ''}
      style={{ width: fullWidth ? '100%' : 'auto' }}
      {...{ active, isOutOfStock, primary, secondary, subscribe, isPayPal, isApplePay, unstyled }}
      {...rest}
    />
  );
};

//@ts-ignore
const StyledButton = styled.button(
  ({
    primary,
    secondary,
    subscribe,
    isPayPal,
    isApplePay,
    isGooglePay,
    active,
    isOutOfStock,
    unstyled,
  }: ButtonProps) => [
    tw`flex justify-center px-12 py-2 text-lg border rounded-full lg:text-xl whitespace-nowrap`,
    primary &&
      tw`border-raisin-black hover:bg-raisin-black hover:border-raisin-black hover:text-white`,
    secondary && tw`text-white bg-bluey disabled:opacity-40`,
    subscribe &&
      tw`border-0 bg-light-gray border-raisin-black hover:bg-raisin-black hover:border-raisin-black hover:text-white`,
    isPayPal && tw`text-black bg-paypal`,
    isApplePay && tw`text-black bg-apple-pay`,
    isGooglePay && tw`text-white bg-google-pay`,
    active && !isOutOfStock && tw`text-white bg-raisin-black`,
    unstyled && tw`py-1`,
    active &&
      isOutOfStock &&
      tw`text-gray-700 bg-gray-200 border-gray-200 hover:bg-gray-200 hover:border-gray-200 hover:text-gray-700`,
  ]
);

export default Button;
