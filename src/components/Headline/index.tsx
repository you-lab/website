type HeadlineProps = {
  children: React.ReactNode;
  centered?: boolean;
};

const Headline = ({ children, centered }: HeadlineProps) => {
  return <h3 className={`text-2xl font-bold ${centered ? 'text-center' : ''}`}>{children}</h3>;
};

export default Headline;
