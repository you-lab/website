import Button from '@/components/Button';
import { ReactNode } from 'react';
import { FaApplePay } from 'react-icons/fa';
import { SiGooglepay, SiPaypal } from 'react-icons/si';
type PaymentButton = {
  children: ReactNode;
};

const PaymentButton = ({ children }: PaymentButton) => {
  return children;
};

const ApplePay = () => {
  return (
    <Button fullWidth unstyled className="w-full bg-apple-pay">
      <FaApplePay className="w-10 h-10 text-black" />
    </Button>
  );
};

const GooglePay = () => {
  return (
    <Button fullWidth unstyled className="w-full bg-google-pay">
      <SiGooglepay className="w-10 h-10 text-white" />
    </Button>
  );
};

const PayPal = () => {
  return (
    <Button fullWidth unstyled className="w-full bg-paypal">
      <SiPaypal className="w-10 h-10 text-white" />
    </Button>
  );
};

PaymentButton.ApplePay = ApplePay;
PaymentButton.GooglePay = GooglePay;
PaymentButton.PayPal = PayPal;

export default PaymentButton;
