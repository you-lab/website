import Button from '@/components/Button';
import Headline from '@/components/Headline';
import { useCartContext } from '@/context/CartContext';
import { useCheckoutContext } from '@/context/CheckoutContext';
import useCreateCheckout from '@/hooks/useCreateCheckout';
import { useModal } from '@/hooks/useModal';
import { format } from 'date-fns';
import { ReactComponent as CheckMark } from '../../../public/svg/checkmark.svg';
import Modal from '../Modal';

const Confirmation = () => {
  const { checkoutComplete, setCheckoutComplete, setActiveStep } = useCheckoutContext();
  const { order, resetOrder } = useCartContext();

  const { open, toggle } = useModal(checkoutComplete);

  if (!checkoutComplete) {
    return null;
  }

  /*  const removeStyles = () => {
    const element = document.querySelector('html') as HTMLElement;
    setTimeout(() => {
      element.removeAttribute('style');
    }, 300);
  }; */

  return (
    <Modal
      open={checkoutComplete}
      close={() => {
        setActiveStep('Shipping');
        resetOrder();
        setCheckoutComplete(false);
        /*  removeStyles(); */
      }}
      fullWidth
    >
      <div className="flex flex-col items-center justify-center w-full space-y-10">
        <CheckMark />
        <Headline>Thank You!</Headline>

        <div
          className="p-10 bg-gradient-to-b from-gradient-start to-gradient-end"
          id="ordercompleted"
        >
          <h4>Your order was placed successfully.</h4>
          <p>Check your email for your order confirmation.</p>
        </div>

        <div className="max-w-sm px-10 space-y-4">
          <p>Your Order: {order?.number}</p>
          <p>
            Order Date:{' '}
            {format(new Date(order?.placed_at ? order?.placed_at : ''), 'dd. MMMM yyyy h:m aaaa')}
          </p>

          <p>We have sent the order confirmation details to {order?.customer_email}</p>
        </div>

        <Button
          secondary
          onClick={() => {
            resetOrder();
            setActiveStep('Shipping');
            setCheckoutComplete(false);
            /*   removeStyles(); */
          }}
        >
          Keep shopping
        </Button>
      </div>
    </Modal>
  );
};

export default Confirmation;
