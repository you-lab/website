import Slider, { Settings } from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import { urlFor } from '@/libs/sanity';

const settings: Settings = {
  dots: true,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
};

const ImageGallery = ({ images }: { images: any }) => {
  return (
    <div className="mb-20 w-80 lg:mb-0">
      <Slider {...settings}>
        {images.map((image: any) => (
          <div key={image._key}>
            <img className="aspect-square" width="320px" height="auto" src={urlFor(image).url()}  />
          </div>
        ))}
      </Slider>
    </div>
  );
};

export default ImageGallery;
