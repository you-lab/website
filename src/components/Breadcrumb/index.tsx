import { useCheckoutContext } from '@/context/CheckoutContext';
import { useEffect, useState } from 'react';
import { FiCheck } from 'react-icons/fi';

const Breadcrumb = ({ breadCrumbSteps }: { breadCrumbSteps: String[] }) => {
  const { currentSteps } = useCheckoutContext();

  return (
    <div className="space-y-3">
      <div className="flex justify-between w-full space-x-6">
        {breadCrumbSteps.map((step: any) => (
          <div key={step} className="flex items-center space-x-4">
            <span>{step}</span>
            <FiCheck
              className={`w-6 h-6 ${!currentSteps.includes(step) ? 'invisible' : 'visible'}`}
              strokeWidth={1}
            />
          </div>
        ))}
      </div>
      <div className="relative w-full h-[2px] bg-dark-gray">
        <div
          className={`absolute inset-0 ${
            currentSteps.length === 1 ? 'w-1/3' : currentSteps.length === 2 ? 'w-2/3' : 'w-3/3'
          } h-[2px] bg-bluey`}
        ></div>
      </div>
    </div>
  );
};

export default Breadcrumb;
