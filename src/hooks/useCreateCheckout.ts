import CommerceLayer from '@commercelayer/sdk';
import Cookies from 'js-cookie';
import { useEffect, useState } from 'react';
import { useGetToken } from './useGetToken';

const useCreateCheckout = () => {
  const [orderId, setOrderId] = useState('');

  const token = useGetToken({
    clientId: 'iBRE5rRmpjtgbAx_5JPVjN7GS6KcmPNfJzQXLdBbvsA',
    clientSecret: 'vSWinW7KD7bO0dRFxq7VbnNVvAA5l7PDmXELk5b7z8U',
    endpoint: 'https://bridgemaker.commercelayer.io',
    scope: 'market:9302',
    countryCode: 'GB',
  });

  useEffect(() => {
    const getCookieOrder = Cookies.get(`clOrderId-GB`);

    if (!getCookieOrder && token) {
      createOrder();
    } else {
      setOrderId(getCookieOrder || '');
    }
  }, [token]);

  const createOrder = async () => {
    Cookies.remove('clOrderId-GB');

    const cl = CommerceLayer({
      organization: 'bridgemaker',
      accessToken: token,
    });

    let order: any = null;
    order = await cl.orders.create({ language_code: 'en' });

    order = await cl.orders.update({
      id: order.id,
      privacy_url: 'https://www.weareyoulab.com/',
      return_url: 'https://www.weareyoulab.com/',
      terms_url: 'https://www.weareyoulab.com/',
    });

    setOrderId(order.id);

    Cookies.set(`clOrderId-GB`, order.id as string, {
      expires: 7,
    });
  };

  return { orderId, createOrder };
};

export default useCreateCheckout;
