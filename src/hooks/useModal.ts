import { useState } from 'react';

export const useModal = (state: boolean) => {
  const [open, setOpen] = useState<boolean>(state);
  const toggle = () => setOpen(!open);
  return {
    open,
    toggle,
  };
};
