import React from 'react';
import ReactDOM from 'react-dom';
import { CartProvider } from './context/CartContext';
import App from './App';
import './styles/global.css';
import { CheckoutProvider } from './context/CheckoutContext';

ReactDOM.render(
  <React.StrictMode>
    <CheckoutProvider>
      <CartProvider>
        <App />
      </CartProvider>
    </CheckoutProvider>
  </React.StrictMode>,
  document.getElementById('root')
);
