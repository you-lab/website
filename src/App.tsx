import Button from '@/components/Button';
import Cart from '@/components/Cart';
import Checkout from '@/components/Checkout';
import Confirmation from '@/components/Confirmation';
import Headline from '@/components/Headline';
import Image from '@/components/Image';
import Modal from '@/components/Modal';
import { LiquidIcon, TabletIcon } from '@/components/SVG';
import { useCartContext } from '@/context/CartContext';
import { useCheckoutContext } from '@/context/CheckoutContext';
import { useGetToken } from '@/hooks/useGetToken';
import { useModal } from '@/hooks/useModal';
import { getContent, getProducts, urlFor } from '@/libs/sanity';
import CommerceLayer, { Sku } from '@commercelayer/sdk';
import { RadioGroup } from '@headlessui/react';
import { useEffect, useState } from 'react';
import { FiChevronDown, FiChevronUp } from 'react-icons/fi';
import { v4 as uuidv4 } from 'uuid';
import ImageGallery from './components/ImageGallery';

const App = () => {
  const [products, setProducts] = useState<any>([]);
  const [product, setProduct] = useState('');
  const [selectedProduct, setSelectedProduct] = useState<any>();
  const [content, setContent] = useState<any>();
  const [quantity, setQuantity] = useState(1);
  const [showCart, setShowCart] = useState(false);
  const [preferredModel, setPreferredModel] = useState('');
  const [frequency, setFrequency] = useState('');
  const [images, setImages] = useState();

  const [showFrequency, setShowFequency] = useState(false);
  const { lineItems, addToCart, order } = useCartContext();
  const { startCheckout, checkoutComplete } = useCheckoutContext();

  const { open, toggle } = useModal(false);

  const token = useGetToken({
    clientId: 'iBRE5rRmpjtgbAx_5JPVjN7GS6KcmPNfJzQXLdBbvsA',
    clientSecret: 'vSWinW7KD7bO0dRFxq7VbnNVvAA5l7PDmXELk5b7z8U',
    endpoint: 'https://bridgemaker.commercelayer.io',
    scope: 'market:9302',
    countryCode: 'GB',
  });

  //@ts-ignore
  useEffect(async () => {
    if (localStorage.getItem('user-id') === null) {
      localStorage.setItem('user-id', uuidv4());
    }

    if (token) {
      const cl = CommerceLayer({
        organization: 'bridgemaker',
        accessToken: token,
      });

      cl.skus.list({ include: ['prices', 'stock_items'] }).then((skus) => {
        setProducts(skus);
      });

      //@ts-ignore
      const content = await getContent();

      setContent(content);
    }
  }, [token]);

  useEffect(() => {
    const showCart = async () => {
      if (order?.skus_count! > 0) {
        setShowCart(true);
      }
    };

    const cartWrapper = document.querySelector('.navigation_mini-cart-wrapper');

    cartWrapper?.addEventListener('click', showCart);

    return () => cartWrapper?.removeEventListener('click', showCart);
  });

  useEffect(() => {
    if (lineItems && lineItems?.length > 0) {
      setShowCart(true);
    } else {
      setShowCart(false);
    }
  }, [lineItems]);

  //@ts-ignore
  useEffect(async () => {
    if (selectedProduct) {
      const products = await getProducts();

      products.map(
        (product: any) =>
          product.reference === selectedProduct.reference && setImages(product.images)
      );
    }
  }, [selectedProduct]);

  return (
    <div className="container mx-auto">
      <Confirmation />
      <Checkout />
      {products && content && !startCheckout && !checkoutComplete && (
        <div className="max-w-xs mx-auto lg:max-w-sm">
          <Headline centered>Our Products</Headline>
          <div className="flex flex-col mt-10 lg:flex-row lg:items-center lg:justify-center lg:space-x-10">
            {selectedProduct && images ? (
              <ImageGallery images={images} />
            ) : (
              <Image url={urlFor(content[0].default_product_image).url()} />
            )}
            <div className="space-y-6">
              <div className="space-y-10 lg:w-[350px]">
                <RadioGroup
                  className="flex justify-center space-x-4"
                  value={product}
                  onChange={setProduct}
                >
                  {products.map((product: Sku) => (
                    <div key={product.code}>
                      <RadioGroup.Option value={product.name}>
                        {({ checked }) => (
                          <>
                            {product.code === 'LIQUID2022XXX' && (
                              <LiquidIcon
                                active={checked}
                                className="cursor-pointer"
                                onClick={() => {
                                  setQuantity(1);
                                  setSelectedProduct(product);
                                }}
                              />
                            )}

                            {product.code === 'TABLETS2022XXX' && (
                              <TabletIcon
                                active={checked}
                                className="cursor-pointer"
                                onClick={() => {
                                  setQuantity(1);
                                  setSelectedProduct(product);
                                }}
                              />
                            )}
                          </>
                        )}
                      </RadioGroup.Option>
                    </div>
                  ))}
                </RadioGroup>
              </div>
              <div className="space-y-10 lg:w-[350px]">
                {selectedProduct?.name === 'Liquid (1 litre)' && (
                  <p className="text-lg text-center">
                    <strong>Laundry Detergent Liquid</strong> <br /> 120 ml, £ 10{' '}
                  </p>
                )}

                {selectedProduct?.name === 'Tablets (20 tabs)' && (
                  <p className="text-lg text-center">
                    <strong>Laundry Detergent Tablets</strong> <br /> 20 per box, £ 10{' '}
                  </p>
                )}

                <RadioGroup
                  className="space-y-4"
                  value={preferredModel}
                  onChange={setPreferredModel}
                >
                  <RadioGroup.Option value="subscription">
                    {({ checked }) => (
                      <>
                        {!showFrequency ? (
                          <>
                            <Button
                              className="flex items-center button"
                              primary
                              active={checked}
                              onClick={() => setShowFequency(true)}
                              fullWidth
                            >
                              <span>{frequency === '' ? 'Subscribe (-15%/unit)' : frequency}</span>

                              <FiChevronDown className="w-6 h-6 ml-6 text-current" />
                            </Button>
                          </>
                        ) : (
                          <RadioGroup
                            className="p-6 space-y-4 border rounded-3xl border-raisin-black"
                            value={frequency}
                            onChange={setFrequency}
                          >
                            <RadioGroup.Label
                              className="flex items-center justify-center text-lg lg:text-xl"
                              onClick={() => {
                                setShowFequency(false);
                                setPreferredModel('');
                              }}
                            >
                              <span>Subscribe (-15%/unit)</span>

                              <FiChevronUp className="w-6 h-6 ml-6 text-raisin-black" />
                            </RadioGroup.Label>

                            <RadioGroup.Label className="block mt-0 text-center">
                              How often do you wash?
                            </RadioGroup.Label>
                            <RadioGroup.Option value="3 boxes every month">
                              {({ checked }) => (
                                <Button
                                  subscribe
                                  fullWidth
                                  active={checked}
                                  onClick={() => {
                                    toggle();
                                    setShowFequency(false);
                                    setPreferredModel('single');
                                  }}
                                >
                                  <span className="text-sm">
                                    {'>'} 1 / day <br /> 3 boxes every month
                                  </span>
                                </Button>
                              )}
                            </RadioGroup.Option>
                            <RadioGroup.Option value="1 box every month">
                              {({ checked }) => (
                                <Button
                                  subscribe
                                  fullWidth
                                  active={checked}
                                  onClick={() => {
                                    toggle();
                                    setShowFequency(false);
                                    setPreferredModel('single');
                                  }}
                                >
                                  <span className="text-sm">
                                    {' '}
                                    2-3 / week <br /> 1 box every month'
                                  </span>
                                </Button>
                              )}
                            </RadioGroup.Option>
                            <RadioGroup.Option value="1 box every 2 months">
                              {({ checked }) => (
                                <Button
                                  subscribe
                                  fullWidth
                                  active={checked}
                                  onClick={() => {
                                    toggle();
                                    setShowFequency(false);
                                    setPreferredModel('single');
                                  }}
                                >
                                  <span className="text-sm">
                                    Once a week <br /> 1 box every 2 months
                                  </span>
                                </Button>
                              )}
                            </RadioGroup.Option>
                          </RadioGroup>
                        )}
                      </>
                    )}
                  </RadioGroup.Option>

                  <RadioGroup.Option value="single">
                    {({ checked }) => (
                      <Button
                        fullWidth
                        primary
                        active={checked}
                        onClick={() => setShowFequency(false)}
                      >
                        Single
                      </Button>
                    )}
                  </RadioGroup.Option>
                </RadioGroup>
              </div>
              <p className="mt-4 text-sm text-gray-600">{content[0].packaging}</p>

              <div className="space-y-6">
                <div className="flex flex-col space-y-6">
                  <Button
                    id="add-to-cart"
                    secondary
                    onClick={() => {
                      addToCart(selectedProduct, quantity);
                    }}
                    disabled={preferredModel === '' || product === ''}
                  >
                    Add to Cart {selectedProduct ? '— ' : ''}
                    {selectedProduct?.prices[0].formatted_compare_at_amount}
                  </Button>
                </div>
              </div>
            </div>
          </div>

          {showCart && <Cart close={() => setShowCart(false)} />}
          <Modal
            open={open}
            close={toggle}
            title="Coming soon…"
            description="We are not quite there yet to offer subscriptions, but you can sign up via email to be the first to hear once subscription is available too."
            large
          >
            <div className="flex flex-col items-center justify-center max-w-sm mx-auto space-y-6">
              <div className="relative grid w-full floating-input">
                <input
                  className={`w-full h-12 p-3 focus:outline-none focus:border-bluey focus:shadow-sm border-light-gray `}
                  type="text"
                  placeholder="Email Address"
                />
                <label
                  htmlFor="emailAddress"
                  className="absolute top-0 left-0 h-full px-3 py-3 transition-all duration-100 ease-in-out origin-left transform pointer-events-none"
                >
                  Email Address
                </label>
              </div>
              <Button type="submit" secondary>
                Submit
              </Button>
              <div className="flex items-center mb-4">
                <label className="inline-flex items-center text-sm font-medium text-left text-mid-gray">
                  <input
                    type="checkbox"
                    className="rounded-full focus:ringfocus:ring-offset-0 focus:ring-bluey focus:ring-opacity-0"
                    required
                  />
                   <span className="ml-2">
                   I hereby accept the
                    <a href="https://www.weareyoulab.com/terms-and-conditions" className="underline cursor-pointer">
                    terms and conditions
                    </a>
                  </span>
                </label>
              </div>
            </div>
          </Modal>
        </div>
      )}
    </div>
  );
};

export default App;
