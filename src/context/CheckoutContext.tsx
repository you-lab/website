import { createContext, FC, useContext, useEffect, useState } from 'react';

type CheckoutInterface = {
  activeStep: CheckoutStep;
  steps: CheckoutStep[];
  setActiveStep: (step: CheckoutStep) => void;
  currentSteps: CheckoutStep[];
  isEdit: boolean;
  setIsEdit: (edit: boolean) => void;
  formData: CheckoutFormData | undefined;
  selectedPayment: CheckoutPayment | undefined;
  saveFormData: (data: CheckoutFormData) => void;
  savePayment: (data: CheckoutPayment) => void;
  setCheckoutComplete: (complete: boolean) => void;
  checkoutComplete: boolean;
  startCheckout: boolean;
  setStartCheckout: (start: boolean) => void;
};

export type CheckoutFormData = {
  firstName: string;
  lastName: string;
  address: string;
  aptFloorSuite: string;
  city: string;
  state: string;
  zipCode: string;
  phone: string;
};

export type CheckoutPayment = {
  name: string;
  icon: React.ReactNode;
};

const initialState: CheckoutInterface = {
  activeStep: 'Shipping',
  steps: ['Shipping', 'Payment', 'Review'],
  currentSteps: ['Shipping'],
  setActiveStep: () => undefined,
  setIsEdit: () => undefined,
  isEdit: false,
  formData: {
    firstName: '',
    lastName: '',
    address: '',
    aptFloorSuite: '',
    city: '',
    state: '',
    zipCode: '',
    phone: '',
  },
  selectedPayment: undefined,
  saveFormData: () => undefined,
  savePayment: () => undefined,
  checkoutComplete: false,
  setCheckoutComplete: () => undefined,
  startCheckout: false,
  setStartCheckout: () => undefined,
};

const STEPS: CheckoutStep[] = ['Shipping', 'Payment', 'Review'];

export type CheckoutStep = 'Shipping' | 'Payment' | 'Review';

const CheckoutContext = createContext<CheckoutInterface>(initialState);

export const CheckoutProvider: FC = ({ children }) => {
  const [activeStep, setActiveStep] = useState<CheckoutStep>('Shipping');
  const [currentSteps, setCurrentSteps] = useState<CheckoutStep[]>([]);
  const steps: CheckoutStep[] = STEPS;
  const [isEdit, setIsEdit] = useState<boolean>(false);
  const [formData, setFormData] = useState<CheckoutFormData>();
  const [selectedPayment, setSelectedPayment] = useState<CheckoutPayment>();
  const [checkoutComplete, setCheckoutComplete] = useState<boolean>(false);
  const [startCheckout, setStartCheckout] = useState<boolean>(false);

  useEffect(() => {
    let removeLastStep: CheckoutStep[] = currentSteps.slice();

    if (currentSteps.includes(activeStep)) {
      if (activeStep !== 'Shipping') {
        removeLastStep.pop();
        setCurrentSteps(removeLastStep);
      } else {
        setCurrentSteps(['Shipping']);
      }
    } else {
      setCurrentSteps([...currentSteps, activeStep]);
    }
  }, [activeStep]);

  const saveFormData = (data: CheckoutFormData) => {
    setFormData(data);
  };

  const savePayment = (data: CheckoutPayment) => {
    setSelectedPayment(data);
  };

  return (
    <CheckoutContext.Provider
      value={{
        activeStep,
        steps,
        currentSteps,
        setActiveStep,
        isEdit,
        setIsEdit,
        formData,
        selectedPayment,
        saveFormData,
        savePayment,
        checkoutComplete,
        setCheckoutComplete,
        startCheckout,
        setStartCheckout,
      }}
    >
      {children}
    </CheckoutContext.Provider>
  );
};

export const useCheckoutContext = (): CheckoutInterface => useContext(CheckoutContext);
