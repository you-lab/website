import useCreateCheckout from '@/hooks/useCreateCheckout';
import { useGetToken } from '@/hooks/useGetToken';
import CommerceLayer, { CommerceLayerClient, Coupon, LineItem, Order } from '@commercelayer/sdk';
import { createContext, FC, useContext, useEffect, useState } from 'react';
import { CheckoutFormData } from './CheckoutContext';

type CartInterface = {
  lineItems: LineItem[] | undefined;
  order: Order | undefined;
  couponCodes: Coupon[] | undefined;
  stripePaymentClientSecret: string | undefined;
  paymentSuccessFull: boolean | undefined;
  getOrder: () => void;
  getLineItems: () => void;
  addToCart: (selectedProduct: any, quantity: number) => void;
  updateQuantity: (id: string, quantity: number) => void;
  removeLineItem: (id: string) => void;
  checkout: () => void;
  applyCustomerData: (formData: CheckoutFormData, email: string) => void;
  proceedToCheckout: () => void;
  applyCoupon: (coupon: string) => void;
  removeCoupon: () => void;
  resetOrder: () => void;
  prepareStripePayment: () => void;
  confirmPaymentIntent: () => void;
};

export type CartItems = {
  id: string;
  sku_code: string;
  name: string;
  quantity: number;
  image_url: string;
  formatted_total_amount: string;
  description: string;
};

const initialState: CartInterface = {
  lineItems: [],
  order: undefined,
  couponCodes: [],
  stripePaymentClientSecret: '',
  paymentSuccessFull: undefined,
  getOrder: () => undefined,
  getLineItems: () => undefined,
  addToCart: () => undefined,
  updateQuantity: () => undefined,
  removeLineItem: () => undefined,
  checkout: () => undefined,
  applyCustomerData: () => undefined,
  proceedToCheckout: () => undefined,
  applyCoupon: () => undefined,
  removeCoupon: () => undefined,
  resetOrder: () => undefined,
  prepareStripePayment: () => undefined,
  confirmPaymentIntent: () => undefined,
};

const CartContext = createContext<CartInterface>(initialState);

export const CartProvider: FC = ({ children }) => {
  const [lineItems, setLineItems] = useState<LineItem[]>();
  const [couponCodes, setCouponCodes] = useState<Coupon[]>();
  const [order, setOrder] = useState<Order>();
  const [stripePaymentClientSecret, setStripePaymentClientSecret] = useState<string>();
  const [paymentSuccessFull, setPaymentSuccessFull] = useState<boolean>(false);
  const [stripeId, setStripeId] = useState<string>()
  const token = useGetToken({
    clientId: 'iBRE5rRmpjtgbAx_5JPVjN7GS6KcmPNfJzQXLdBbvsA',
    clientSecret: 'vSWinW7KD7bO0dRFxq7VbnNVvAA5l7PDmXELk5b7z8U',
    endpoint: 'https://bridgemaker.commercelayer.io',
    scope: 'market:9302',
    countryCode: 'GB',
  });

  const { orderId, createOrder } = useCreateCheckout();

  let cl: CommerceLayerClient;

  if (token) {
    cl = CommerceLayer({
      organization: 'bridgemaker',
      accessToken: token,
    });
  }

  useEffect(() => {
    const getCouponCodes = async () => {
      const coupons = await cl.coupons.list();
      setCouponCodes(coupons);
    };

    if (orderId) {
      getCouponCodes();
    }
  }, [orderId]);

  useEffect(() => {
    const updateOrder = async () => {
      await getOrder();
    };

    if (orderId) {
      updateOrder();
    }
  }, [lineItems]);

  useEffect(() => {
    const initCartCounter = async () => {
      const cartCounter = document.querySelector('.navigation_mini-cart-counter') as HTMLElement;
      const order = await getOrder();

      if (cartCounter && order) {
        cartCounter.innerHTML = order.skus_count?.toString()!;
      }
    };

    if (orderId) {
      initCartCounter();
    }
  }, [orderId, lineItems]);

  const getOrder = async () => {
    const order = await cl.orders.retrieve(orderId, { include: ['line_items'] });
    setOrder(order);
    return order;
  };

  const getLineItems = async () => {
    const order = await cl.orders.retrieve(orderId, { include: ['line_items'] });
    setLineItems(order.line_items);
    return order.line_items;
  };

  const updateQuantity = async (id: string, quantity: number) => {
    await cl.line_items.update({ id: id, quantity });
    const updateItems = await getLineItems();
    setLineItems(updateItems);
  };

  const removeLineItem = async (id: string) => {
    await cl.line_items.delete(id);
    const updateItems = await getLineItems();
    setLineItems(updateItems);
  };

  const proceedToCheckout = async () => {
    const orderInclude = await cl.orders.retrieve(orderId, { include: ['line_items'] });

    window.location.href = `https://checkout.weareyoulab.com/${orderInclude.id}?accessToken=${token}`;
  };

  const addToCart = async (selectedProduct: any, quantity: number) => {
    const line_items = await getLineItems();

    const currentLineItem = line_items?.filter((item) => item.sku_code === selectedProduct.code);

    if (currentLineItem !== undefined && currentLineItem.length > 0) {
      await cl.line_items.update({ id: currentLineItem[0].id });
    } else {
      await cl.line_items.create({
        sku_code: selectedProduct.code,
        quantity,
        order: { type: 'orders', id: orderId },
      });
    }

    const updateItems = await getLineItems();

    setLineItems(updateItems);
  };

  const applyCoupon = async (coupon: string) => {
    const order = await cl.orders.update({ id: orderId, gift_card_or_coupon_code: coupon });

    setOrder(order);
  };

  const removeCoupon = async () => {
    const order = await cl.orders.update({ id: orderId, coupon_code: '' });

    setOrder(order);
  };

  const applyCustomerData = async (formData: CheckoutFormData, email: string) => {
    await cl.orders.update({ id: orderId, customer_email: email });

    const address = await cl.addresses.create({
      first_name: formData?.firstName,
      last_name: formData?.lastName,
      line_1: formData?.address!,
      city: formData?.city!,
      zip_code: formData?.zipCode!,
      state_code: 'UK',
      country_code: 'GB',
      phone: formData?.phone!,
    });

    await cl.orders.update({
      id: orderId,
      billing_address: {
        type: 'addresses',
        id: address.id,
      },
    });

    await cl.orders.update({
      id: orderId,
      shipping_address: {
        type: 'addresses',
        id: address.id,
      },
    });

    const order = await cl.orders.retrieve(orderId, {
      include: ['shipments'],
    });

    await cl.shipments.update({
      //@ts-ignore
      id: order.shipments[0].id as string,
      shipping_method: { type: 'shipping_methods', id: 'KNYKGFYvLE' },
    });

    const updatedOrder = await cl.orders.retrieve(orderId);

    setOrder(updatedOrder);
  };

  const checkout = async () => {
    await cl.orders.update({
      id: orderId,
      payment_source: {type: "stripe_payments", id: stripeId!},
   }) 

    await cl.orders.update({
      id: orderId,
      _save_payment_source_to_customer_wallet: true,
    });

    const placedOrder = await cl.orders.update({
      id: orderId,
      _place: true,
    });

    setOrder(placedOrder);
  };

  const resetOrder = () => {
    setOrder(undefined);
    setLineItems(undefined);
    createOrder();
  };


  const prepareStripePayment = async () => {
    await cl.orders.update({
      id: orderId,
      payment_method: cl.payment_methods.relationship('eEyvxsXpJm'),
    });

    const stripePayment = await cl.stripe_payments.create({
      order: cl.orders.relationship(orderId), 
    });

    setStripeId(stripePayment.id)

    await cl.orders.retrieve(orderId)

    setStripePaymentClientSecret(stripePayment.client_secret);
    
  };

  const confirmPaymentIntent = async() => {
  
    setPaymentSuccessFull(true);
  };

  return (
    <CartContext.Provider
      value={{
        order,
        lineItems,
        couponCodes,
        getLineItems,
        getOrder,
        updateQuantity,
        removeLineItem,
        checkout,
        applyCustomerData,
        addToCart,
        proceedToCheckout,
        applyCoupon,
        removeCoupon,
        resetOrder,
        prepareStripePayment,
        stripePaymentClientSecret,
        paymentSuccessFull,
        confirmPaymentIntent,
      }}
    >
      {children}
    </CartContext.Provider>
  );
};

export const useCartContext = (): CartInterface => useContext(CartContext);
