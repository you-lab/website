import { v4 as uuidv4 } from 'uuid';

export const userTracking = async (data: any) => {
  await fetch('https://hook.eu1.make.com/tm4epcwziywb64qk6dwdm74kyr4h24in', {
    method: 'POST',
    mode: 'cors',
    cache: 'no-cache',
    credentials: 'same-origin',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify({
      id: localStorage.getItem('user-id') ? localStorage.getItem('user-id') : uuidv4(),
      name: data?.name,
      description: data?.description,
      image: data?.image_url,
      price: data?.prices[0].formatted_compare_at_amount,
    }),
  });
};
