import sanityClient, { SanityClient } from '@sanity/client';
import imageUrlBuilder from '@sanity/image-url';

const client: SanityClient = sanityClient({
  projectId: 'yumws2sv',
  dataset: 'production',
  apiVersion: '2021-03-25',
  token:
    'skh4EfcCrS3VbdubJLtz3vVz8bpBYnT65vy4ZBnB7QGsQTJplaQLgCvNexcmRSfyW0IvEZY9pUUDehBhnl0GyiQ6uOthrEXAYGE2SzPvsCbzh3Zd1Wll3Tk1RsroKeQGVg1q1qcKcgbkSX1YXkUsU4iXtiWlalSyBAxgD67T7uqfjocaJ7Ie', // or leave blank for unauthenticated usage
  useCdn: false,
  ignoreBrowserTokenWarning: true,
});

const builder = imageUrlBuilder(client);

export const urlFor = (source: any) => {
  return builder.image(source);
};

export const getContent = () => client.fetch(`*[_type == "content"]`);
export const getProducts = () => client.fetch(`*[_type == "product"]`);
