module.exports = {
  content: ['./src/**/*.{js,ts,jsx,tsx}'],
  theme: {
    extend: {
      colors: {
        bluey: '#5277D7',
        'light-gray': '#EFEFEF',
        'mid-gray': '#999999',
        'dark-gray': '#EFEFEF',
        'raisin-black': '#262626',
        paypal: '#F7C557',
        'apple-pay': '#EFEFEF',
        'google-pay': '#262626',
        'gradient-start': '#BAECFD',
        'gradient-end': '#FDFFDC',
      },
    },
  },
  plugins: [require('@tailwindcss/forms')],
};
