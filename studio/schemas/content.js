export default {
  title: 'Content',
  name: 'content',
  type: 'document',
  fields: [
    {
      title: 'Step one Text',
      name: 'step_one',
      type: 'string',
    },
    {
      title: 'Step two Text',
      name: 'step_two',
      type: 'string',
    },
    {
      title: 'Checkout Next Button Text',
      name: 'checkout_next_button',
      type: 'string',
    },
    {
      title: 'Checkout Buy Button Text',
      name: 'checkout_buy_button',
      type: 'string',
    },
    {
      title: 'Notify Button Text',
      name: 'notify_button',
      type: 'string',
    },
    {
      title: 'Out of stock Text',
      name: 'out_of_stock',
      type: 'string',
    },
    {
      title: 'Packaging Text',
      name: 'packaging',
      type: 'string',
    },
    {
      title: 'Default Product Image',
      name: 'default_product_image',
      type: 'image',
    },
    {
      title: 'Modal Title',
      name: 'modal_title',
      type: 'string',
    },
    {
      title: 'Modal Description',
      name: 'modal_description',
      type: 'string',
    },
    {
      title: 'Modal Button',
      name: 'modal_button',
      type: 'string',
    },
    {
      title: 'Modal note',
      name: 'modal_note',
      type: 'string',
    },
  ],
};
