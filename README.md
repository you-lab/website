This is a [Vitejs](https://vitejs.dev/) project bootstrapped with [`@vitejs/plugin-react`](https://github.com/vitejs/vite/tree/main/packages/plugin-react).

## Getting Started 🚀

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `src/App.tsx`. The page auto-updates as you edit the file.

## Start Sanity

```bash
npm run start-sanity
# or
yarn start-sanity
```

## Deploy Sanity

```bash
npm run deploy-sanity
# or
yarn deploy-sanity
```

## Deploy on Webflow

First, run the production build:

```bash
npm run build
# or
yarn build

git push
```


## Todos:

- Fix Stripe Transaction and order amounts mismatch
- Fix Dynamic Slider Image Bug (See Image below)

![Image Slider Bug](/public/img/slider-bug.png)
